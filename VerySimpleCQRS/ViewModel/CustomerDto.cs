﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VerySimpleCQRS.ViewModel
{
	public class CustomerDto
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public float OrderTotal { get; set; }

		public List<CustomerOrderDto> Orders { get; private set; }

		public CustomerDto()
		{
			this.Orders = new List<CustomerOrderDto>();
		}

		public override string ToString()
		{
			var sb = new StringBuilder();

			sb.AppendFormat("CUSTOMER{0}---------------{0}", Environment.NewLine);

			sb.AppendFormat("id: {0}{1}", this.Id,Environment.NewLine)
			  .AppendFormat("name: {0}{1}", this.Name,Environment.NewLine)
			  .AppendFormat("order total: {0}{1}", this.OrderTotal,Environment.NewLine)
			  .Append("ORDERS").Append(Environment.NewLine)
			  .Append("---------------").Append(Environment.NewLine);

			foreach (var order in this.Orders)
			{
				sb.AppendFormat("\tOrder id: {0}{1}", order.Id, Environment.NewLine)
				  .AppendFormat("\tPrice: {0:N2}{1}", order.Price, Environment.NewLine);
			}

			sb.Append(Environment.NewLine);

			return sb.ToString();
		}
	}

	public class CustomerOrderDto
	{
		public string Id { get; set; }
		public float Price { get; set; }
	}
}
