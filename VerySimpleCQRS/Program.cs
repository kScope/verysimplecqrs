﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VerySimpleCQRS.Infrastructure;
using VerySimpleCQRS.Model;
using VerySimpleCQRS.Handlers;
using VerySimpleCQRS.ViewModel;

namespace VerySimpleCQRS
{
	class Program
	{
		static void Main(string[] args)
		{

			var messageBus = new Bus();

			var eventsStore = new EventStore(messageBus);
			var customersRepository = new ModelRepository<Customer>( eventsStore );
			

			var customerCommandsHandler = new CustomerCommandHandler( customersRepository );
			messageBus.RegisterHandler<CreateCustomer>(customerCommandsHandler.Handle);
			messageBus.RegisterHandler<ChangeCustomerName>(customerCommandsHandler.Handle);
			messageBus.RegisterHandler<AddOrderToCustomer>(customerCommandsHandler.Handle);


			var readRepository = new ReadRepository<CustomerDto>();
			var customerEventsHandler = new CustomerEventHandler(readRepository);
			messageBus.RegisterHandler<CustomerCreated>(customerEventsHandler.Handle);
			messageBus.RegisterHandler<CustomerNameChanged>(customerEventsHandler.Handle);
			messageBus.RegisterHandler<CustomerOrderAdded>(customerEventsHandler.Handle);
			

			var customerId = "123-456-789";
			var customerName = "Customer Name";
			var newCustomerName = "Customer 1";

			messageBus.Send(new CreateCustomer(customerId, customerName));
			messageBus.Send(new ChangeCustomerName(customerId, newCustomerName));
			messageBus.Send(new AddOrderToCustomer(customerId, "ORDER_001", 100.0f));
			messageBus.Send(new AddOrderToCustomer(customerId, "ORDER_002", 30.0f));
			messageBus.Send(new AddOrderToCustomer(customerId, "ORDER_003", 10.0f));


			customerId = "456-789-123";
			customerName = "Customer 2";

			messageBus.Send(new CreateCustomer(customerId, customerName));
			messageBus.Send(new AddOrderToCustomer(customerId, "ORDER_004", 123.456f));
			messageBus.Send(new AddOrderToCustomer(customerId, "ORDER_005", 56.0f));
			messageBus.Send(new AddOrderToCustomer(customerId, "ORDER_006", 13.0f));

			foreach (var customer in readRepository.All())
			{
				Console.WriteLine(customer);
			}

			

			Console.ReadLine();

		}
	}
}
