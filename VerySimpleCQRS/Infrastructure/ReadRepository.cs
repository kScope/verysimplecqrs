﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VerySimpleCQRS.Infrastructure
{
	public interface IReadRepository<T>
	{
		T GetById(string id);
		IEnumerable<T> All();
		void Add(string id, T item);
		void Delete(string itemId);
	}

	public class ReadRepository<T>: IReadRepository<T>
	{
		Dictionary<string,T> _dataList = new Dictionary<string, T>();

		#region IReadRepository<T> Members

		public T GetById(string id)
		{
			return _dataList[id];
		}

		public IEnumerable<T> All()
		{
			return _dataList.Values;
		}

		public void Add(string id, T item)
		{
			_dataList[id] = item;
		}

		public void Delete(string itemId)
		{
			_dataList.Remove(itemId);
		}

		#endregion
	}
}
