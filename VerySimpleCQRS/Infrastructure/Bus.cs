﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VerySimpleCQRS.Infrastructure
{
	public class Bus : ICommandSender , IEventPublisher
	{
		private readonly Dictionary<Type, List<Action<Message>>> _routes = new Dictionary<Type, List<Action<Message>>>();

		public void RegisterHandler<T>(Action<T> handler) where T : Message
		{
			List<Action<Message>> handlersList = null;

			if (!_routes.TryGetValue(typeof(T), out handlersList))
			{
				handlersList = new List<Action<Message>>();

				_routes.Add(typeof(T), handlersList);
			}

			handlersList.Add( DelegateAdjuster.CastArgument<Message, T>( x => handler(x) ));

		}

		#region IEventPublisher Members

		public void Publish<T>(T @event) where T : Event
		{
			List<Action<Message>> handlersList = null;

			if (!_routes.TryGetValue(@event.GetType(), out handlersList)) return;

			foreach (var handler in handlersList)
				handler(@event);
		}

		#endregion

		#region ICommandSender Members

		public void Send<T>(T command) where T : Command
		{
			List<Action<Message>> handlersList = null;

			if (!_routes.TryGetValue(command.GetType(), out handlersList))
				throw new InvalidOperationException("Nessun handler registrato per l'evento " + typeof(T));

			if( handlersList.Count != 1 )
				throw new InvalidOperationException("Il comando " + typeof(T) + " non può essere inviato a più di un handler ");

			handlersList[0](command);
		}

		#endregion
	}
}
