﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VerySimpleCQRS.Infrastructure
{
	public interface IModelRepository<T> where T : AggregateRoot
	{
		void Save(T aggregateRoot);
		T GetById(string id);
	}

	public class ModelRepository<T>: IModelRepository<T> where T: AggregateRoot, new()
	{
		IEventStore _eventStore;

		public ModelRepository(IEventStore eventStore)
		{
			_eventStore = eventStore;
		}

		#region IRepository<T> Members

		public void Save(T aggregateRoot)
		{
			_eventStore.Save(aggregateRoot.Id, aggregateRoot.GetUncommittedChanges());

			aggregateRoot.CommitChanges();
		}

		public T GetById(string id)
		{
			var events = _eventStore.GetEventsForAggregate(id);
			var obj = new T();

			obj.LoadFromHistory(events);

			return obj;
		}

		#endregion
	}
}
