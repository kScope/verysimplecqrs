﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VerySimpleCQRS.Infrastructure
{
	public interface IEventStore
	{
		void Save(string aggregateRootId, IEnumerable<Event> events);
		IEnumerable<Event> GetEventsForAggregate(string aggregateRootId);
	}

	public class EventStore : IEventStore
	{
		IEventPublisher _publisher;
		private readonly Dictionary<string, List<EventDescriptor>> _current = new Dictionary<string, List<EventDescriptor>>();

		private struct EventDescriptor
		{
			public readonly Event EventData;
			public readonly string Id;
			public readonly int Version;

			public EventDescriptor(string id, Event eventData, int version)
			{
				EventData = eventData;
				Version = version;
				Id = id;
			}
		}

		public EventStore(IEventPublisher publisher)
		{
			_publisher = publisher;
		}

		#region IEventStore Members

		public void Save(string aggregateRootId, IEnumerable<Event> events)
		{
			List<EventDescriptor> eventsInStore;

			if (!_current.TryGetValue(aggregateRootId, out eventsInStore))
			{
				eventsInStore = new List<EventDescriptor>();
				_current.Add(aggregateRootId, eventsInStore);
			}

			foreach (var @event in events)
			{
				eventsInStore.Add( new EventDescriptor( aggregateRootId, @event , -1));

				_publisher.Publish(@event);
			}
		}

		public IEnumerable<Event> GetEventsForAggregate(string aggregateRootId)
		{
			List<EventDescriptor> eventsInStore;

			if (!_current.TryGetValue(aggregateRootId, out eventsInStore))
			{
				throw new InvalidOperationException("Aggregate root non trovato");
			}

			return eventsInStore.Select( x => x.EventData);
		}

		#endregion
	}

}
