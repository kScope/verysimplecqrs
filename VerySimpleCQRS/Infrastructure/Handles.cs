﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VerySimpleCQRS.Infrastructure
{
	interface Handles<T> where T:Message
	{
		void Handle(T message);
	}
}
