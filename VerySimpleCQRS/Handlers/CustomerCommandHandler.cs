﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VerySimpleCQRS.Infrastructure;
using VerySimpleCQRS.Model;

namespace VerySimpleCQRS.Handlers
{
	public class CustomerCommandHandler : Handles<CreateCustomer>, Handles<ChangeCustomerName>, Handles<AddOrderToCustomer>
	{
		IModelRepository<Customer> _repository;

		public CustomerCommandHandler(IModelRepository<Customer> repository)
		{
			_repository = repository;
		}

		#region Handles<CreateCustomer> Members

		public void Handle(CreateCustomer message)
		{
			var customer = new Customer( message.Id, message.Name);

			_repository.Save(customer);
		}

		#endregion

		#region Handles<ChangeCustomerName> Members

		public void Handle(ChangeCustomerName message)
		{
			var customer = _repository.GetById(message.Id);

			customer.ChangeName(message.NewName);

			_repository.Save( customer );
		}

		#endregion

		#region Handles<AddOrderToCustomer> Members

		public void Handle(AddOrderToCustomer message)
		{
			var customer = _repository.GetById(message.CustomerId);

			customer.AddOrder(message.OrderId, message.OrderPrice);

			_repository.Save(customer);
		}

		#endregion
	}
}
