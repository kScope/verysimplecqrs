﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VerySimpleCQRS.Infrastructure;
using VerySimpleCQRS.Model;
using VerySimpleCQRS.ViewModel;
using System.Diagnostics;

namespace VerySimpleCQRS.Handlers
{
	public class CustomerEventHandler: Handles<CustomerCreated>, Handles<CustomerNameChanged>, Handles<CustomerOrderAdded>
	{
		IReadRepository<CustomerDto> _repository;

		public CustomerEventHandler(IReadRepository<CustomerDto> repository)
		{
			_repository = repository;
		}

		#region Handles<CustomerCreated> Members

		public void Handle(CustomerCreated message)
		{
			Debug.WriteLine("New customer created");

			var customer = new CustomerDto
			{
				Id = message.Id,
				Name = message.Name
			};

			_repository.Add(message.Id, customer);
		}

		#endregion

		#region Handles<CustomerNameChanged> Members

		public void Handle(CustomerNameChanged message)
		{
			Debug.WriteLine("Customer has changed his name");

			var customer = _repository.GetById(message.Id);

			customer.Name = message.NewName;
		}

		#endregion

		#region Handles<CustomerOrderAdded> Members

		public void Handle(CustomerOrderAdded message)
		{
			Debug.WriteLine("Customer  has a new order");

			var customer = _repository.GetById(message.CustomerId);

			customer.Orders.Add(new CustomerOrderDto
			{
				Id = message.OrderId,
				Price = message.OrderPrice
			});

			customer.OrderTotal += message.OrderPrice;
		}

		#endregion

	}
}
