﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VerySimpleCQRS.Infrastructure;

namespace VerySimpleCQRS.Model
{
	public class Customer : AggregateRoot
	{
		string _id;
		string _name;
		List<KeyValuePair<string,float>> _orders = new List<KeyValuePair<string,float>>();
		float _ordersTotal;

		public override string Id
		{
			get { return _id; }
		}

		private void Apply(CustomerCreated @event)
		{
			this._id = @event.Id;
			this._name = @event.Name;
		}

		private void Apply(CustomerNameChanged @event)
		{
			this._name = @event.NewName;
		}

		private void Apply(CustomerOrderAdded @event)
		{
			_orders.Add(new KeyValuePair<string, float>(@event.OrderId, @event.OrderPrice));
			
			_ordersTotal += @event.OrderPrice;
		}

		public Customer()
		{

		}
		public Customer(string id, string name)
		{
			ApplyChange( new CustomerCreated( id, name ));
		}

		public void ChangeName(string newName)
		{
			ApplyChange(new CustomerNameChanged(this.Id, newName));
		}

		internal void AddOrder(string orderId, float price)
		{
			ApplyChange(new CustomerOrderAdded(this.Id, orderId, price));
		}
	}
}
