﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VerySimpleCQRS.Infrastructure;

namespace VerySimpleCQRS.Model
{
	public class CustomerCreated : Event
	{
		public readonly string Id;
		public readonly string Name;

		public CustomerCreated(string id, string name)
		{
			this.Id = id;
			this.Name = name;
		}
	}

	public class CustomerNameChanged : Event
	{
		public readonly string Id;
		public readonly string NewName;

		public CustomerNameChanged(string id, string newName)
		{
			this.Id = id;
			this.NewName = newName;
		}
	}

	public class CustomerOrderAdded : Event
	{
		public readonly string CustomerId;
		public readonly string OrderId;
		public readonly float OrderPrice;

		public CustomerOrderAdded(string customerId, string orderId, float orderPrice)
		{
			this.CustomerId = customerId;
			this.OrderId = orderId;
			this.OrderPrice = orderPrice;
		}
	}

	public class CustomerOrdersTotalChanged : Event
	{
		public readonly string CustomerId;
		public readonly float TotalPrice;

		public CustomerOrdersTotalChanged(string customerId, float totalPrice)
		{
			this.CustomerId = customerId;
			this.TotalPrice = totalPrice;
		}
	}

}
