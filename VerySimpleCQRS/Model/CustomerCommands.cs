﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VerySimpleCQRS.Infrastructure;

namespace VerySimpleCQRS.Model
{
	public class CreateCustomer : Command
	{
		public readonly string Id;
		public readonly string Name;

		public CreateCustomer(string id, string name)
		{
			this.Id = id;
			this.Name = name;
		}
	}

	public class ChangeCustomerName : Command
	{
		public readonly string Id;
		public readonly string NewName;

		public ChangeCustomerName(string id, string newName)
		{
			this.Id = id;
			this.NewName = newName;
		}
	}

	public class AddOrderToCustomer : Command
	{
		public readonly string CustomerId;
		public readonly string OrderId;
		public readonly float OrderPrice;

		public AddOrderToCustomer(string customerId, string orderId, float orderPrice)
		{
			this.CustomerId = customerId;
			this.OrderId = orderId;
			this.OrderPrice = orderPrice;
		}
	}

}
